import { createRouter, createWebHistory } from 'vue-router'
import Home from "@/views/Home.vue";
import Content from "@/views/Content.vue";
import UserInfo from "@/views/UserInfo.vue";
import Detail from "@/views/Detail.vue";
import Sigin from "@/views/Sigin.vue";
import ModifyInfo from "@/components/ModifyInfo.vue";
import CheckInfo from "@/components/CheckInfo.vue";
import Collects from "@/components/Collects.vue";


const router = createRouter({
  // 配置基础路由
  history: createWebHistory("/video"),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect:"/home",
      children:[
        {
          path:':id',
          component: Content,
          props:true
        },
        {
          path:'detail/:id',
          props:true,
          component: Detail
        },
        {
          path: "userInfo",
          name: "userInfo",
          component: UserInfo,
          redirect:'userInfo/modifyInfo',
          children:[
            {
              path:"modifyInfo",
              name:"modifyInfo",
              component: ModifyInfo
            },
            {
              path:"checkInfo",
              name:"checkInfo",
              component: CheckInfo
            },
            {
              path:"collects",
              name:"collects",
              component: Collects
            }
          ]
        }
      ]
    },
    {
      path:"/sigin",
      name:"sigin",
      component: Sigin
    },


  ]
})

export default router
