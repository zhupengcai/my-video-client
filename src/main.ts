import { createApp } from 'vue'
import MyIcon from "./components/MyIcon.vue";
import "@/assets/icon/iconfont.js"

// 引入UI库
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// 引入根页面
import App from '@/App.vue'
// 引入路由管理
import router from 'router'
// 引入状态管理
import {createPinia} from "pinia";
// 引入状态管理插件
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

// 引入视频播放组件
import vue3videoPlay from 'vue3-video-play' // 引入组件
import 'vue3-video-play/dist/style.css' // 引入css
const pinia =createPinia();
pinia.use(piniaPluginPersistedstate)
const app = createApp(App)
app.component('MyIcon',MyIcon)//全局注册自定义图标组件
app.use(ElementPlus)
app.use(router)
app.use(vue3videoPlay)
app.use(pinia)
app.mount('#app')
