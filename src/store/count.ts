import { defineStore} from "pinia";
import { ref } from "vue";

export const countStore = defineStore("count",()=>{
       const count = ref()
      const setCount=(num)=>{
             count.value = num
       }
       return {
         count,
         setCount
       }
})