import { defineStore } from "pinia";
import { ref } from "vue";

export const userInfoStore = defineStore('userInfo',()=>{
  const userInfo =ref();
  const isLogin = ref(false);
  const setInfo =(data:any)=>{
           userInfo.value=data;
  }

  //修改登录状态
  const setLoginStatus=()=>{
         isLogin.value = !isLogin.value;
     }
     // logout
  const  logout = ()=>{
         isLogin.value =false;
         userInfo.value = "";
     }
  return{
      userInfo,
      isLogin,
      logout,
      setLoginStatus,
      setInfo,
  }
},{
  persist:{
     storage:window.localStorage //持久化配置
  }
})