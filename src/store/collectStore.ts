import { defineStore } from "pinia";
import { computed, ref } from "vue";

export const collectStore =defineStore("collectStore",()=>{

        const data = ref([])
        const setData = (datas)=>{
               data.value =datas
        }
        const total = computed(()=>data.value.length)
        return{
          data,
          setData,
          total
        }


})