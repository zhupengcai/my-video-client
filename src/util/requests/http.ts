import instance from "./index";
import createInstance from "./index";


class http {
    instance = createInstance()
    post(path: string,data:any,params:any): Promise<any> {
        // @ts-ignore
        return new Promise(
            (resolve, reject) => {
                this.instance.post(path,data,{params}).then(
                    (res) => {
                        // console.log(res)
                        // 只返回数据部分
                        resolve(res.data)

                    }
                ).catch(error => {
                    reject(error)
                })
            }
        )
    }

    get(path: string,params:any): Promise<any> {
        // @ts-ignore
        return new Promise(
            (resolve, reject) => {
                // FIXME:get无data参数
                this.instance.get(path,{params:params}).then(
                    (res) => {
                        resolve(res.data)
                    }
                ).catch(error => {
                    reject(error)
                })
            }
        )
    }

    delete(path: string,params:any): Promise<any> {
        // @ts-ignore
        return new Promise(
            (resolve, reject) => {
                this.instance.delete(path,{params:params}).then(
                    (res) => {
                        // console.log(res)
                        resolve(res.data)

                    }
                ).catch(error => {
                    reject(error)
                })
            }
        )
    }

    put(path: string,data:any,params:any) :Promise<any>{
        // @ts-ignore
        return new Promise(
            (resolve, reject) => {
                this.instance.put(path,data,{params:params}).then(
                    (res) => {
                        // console.log(res)
                        resolve(res.data)

                    }
                ).catch(error => {
                    reject(error)
                })
            }
        )
    }

    patch(path: string,data:any,params:any):Promise<any> {
        // @ts-ignore
        return new Promise(
            (resolve, reject) => {
                this.instance.patch(path,data,{params:params}).then(
                    (res) => {
                        // console.log(res)
                        resolve(res.data)

                    }
                ).catch(error => {
                    reject(error)
                })
            }
        )
    }
}

export default http