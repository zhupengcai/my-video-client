import http from "./http";
import * as querystring from "querystring";

class PostRequest extends http{
      Login(params:any){
        return new Promise(
          (resolve,reject)=>{
            super.post("/login",null,params).then(res=>{
                  resolve(res)
            }).catch(e=>{
                  reject(e)
            })
          }
        )
      }
      register(data:any){
           return new Promise(
             (resolve,reject)=>{
               super.post("/user/register",data,null).then(res=>{
                    resolve(res)
               }).catch(e=>{
                 reject(e)
               })
             }
           )
      }
      insertCollect(data){
        return new Promise(
          (resolve,reject)=>{
            super.post("/collect/insert",data,null).then(res=>{
              resolve(res)
            }).catch(e=>{
              reject(e)
            })
          }
        )
      }
}

export default PostRequest