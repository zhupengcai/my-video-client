import axios from "axios";
import { storeToRefs } from "pinia";
import { userInfoStore } from "../../store/userInfo";


const instance = axios.create({
    // baseURL:"http://localhost:8086/videoServer"
    //开发配置
    baseURL:"http://www.zpcnet.top/videoServer"
})

function createInstance(){
    // 创建instance实例，解决store更新而实例配置未更新问题
    let { isLogin, userInfo } = storeToRefs(userInfoStore())
    if(isLogin.value){
        // console.log(userInfo.value.token);
        // console.log(isLogin);
        instance.defaults.headers.common['Authorization'] ="Bearer "+userInfo.value.token
    }
    return instance
}

export default createInstance