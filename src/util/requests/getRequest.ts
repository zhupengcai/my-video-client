import http from "./http";

// @ts-ignore
class GetRequest extends http{
      getAllVideo(){
        return new Promise(
          (resolve,reject)=>{
            super.get("/video/getAllVideo",null).then(
              res=>{
                  resolve(res);
              }
            ).catch(e=>{
              reject(e);
            })
          }
        )
      }

//       /video/getVideoById
  getVideoById(params:any){
    return new Promise(
      (resolve,reject)=>{
        super.get("/video/getVideoById",params).then(
          res=>{
            resolve(res);
          }
        ).catch(e=>{
          reject(e);
        })
      }
    )
  }
  getUserColletct(params:any){
    return new Promise(
      (resolve,reject)=>{
        super.get("/collect/getUserColletct",params).then(
          res=>{
            resolve(res);
          }
        ).catch(e=>{
          reject(e);
        })
      }
    )
  }
  getCollectByIds(params){
        return new Promise(
          (resolve,reject)=>{
            super.get("/collect/getByIds",params).then(
              res=>{
                resolve(res);
              }
            ).catch(e=>{
              reject(e);
            })
          }
        )
  }



}

export default GetRequest