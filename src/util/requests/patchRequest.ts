import http from "./http";

class PatchRequest extends http{
      updateUserInfo(data){
        return new Promise((resolve, reject)=>{
               super.patch("/user/update",data,null).then(res=>{
                 // console.log(res);
                 resolve(res)
               }).catch(error=>{
                  reject(error)
               })
        })
      }
}

export default PatchRequest;