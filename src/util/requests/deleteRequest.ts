import http from "util/requests/http";


class DeleteRequest extends http{
      deleteCollect(params){
              return new Promise((resolve,reject)=>{
                     super.delete("/collect/deleteById",params).then(
                       res=>{
                         resolve(res)
                       }).catch(error=>{
                         reject(error)
                       }
                     )
              })
      }

}

export default DeleteRequest;