# my-video-client
> [MyBlog](https://www.zpcnet.top)


## 介绍
Web端前端视频流应用,Vue3+Element-plus+Vue-router+Vue3-video-player
## 网站架构
![frame](img/frame.png)

## 页面展示
### 1.登录
![img.png](img/Login.png)
### 2.注册
![img.png](img/Register.png)
### 3.首页
![img.png](img/Home.png)
### 4.个人信息页
![img.png](img/UserInfo.png)


## 使用说明

### 1. Project Setup

```sh
npm install
```

### 2. Compile and Hot-Reload for Development

```sh
npm run dev
```

### 3. Type-Check, Compile and Minify for Production

```sh
npm run build
```

### 4. Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### 5. Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

> BUG反馈Email:Zhupc1024@163.com