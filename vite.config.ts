import path from "path"
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
const pathSrc = path.resolve(__dirname,"./src")
const util = path.resolve(__dirname,"src/util")
const store = path.resolve(__dirname,"src/store")
const router = path.resolve(__dirname,"src/router")
// https://vitejs.dev/config/
export default defineConfig({
  //nginx代理配置
  base:"/video",
  plugins: [
    vue(),
  ],
  resolve: {
    alias: {
      '@': pathSrc,
      'util':util,
      'store':store,
      'router':router
    }
  }
})
