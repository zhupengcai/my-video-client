/// <reference types="vite/client" />
// FIXME: Cannot find module ‘@/views/xxx.vue‘ or its corresponding type declarations
declare module '*.vue' {
  import { DefineComponent } from "vue"
  const component: DefineComponent<{}, {}, any>
  export default component
}

declare module 'vue3-video-play'
declare module 'store/*.ts'
